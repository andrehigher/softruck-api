var express    = require('express');

var mongoose = require('mongoose');
var cacheOpts = {
    max:50,
    maxAge:1000*60*2
};
require('mongoose-cache').install(mongoose, cacheOpts);

var Anp = require('../models/anp');

var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    res.header('Access-Control-Allow-Origin', "*")
    next(); // make sure we go to the next routes and don't stop here
});

router.route('/anp')

	// create an anp data (accessed at POST http://localhost:8080/api/anp)
    .post( function (req, res) {
        
        var anp = new Anp( req.body );      	// create a new instance of the Anp model

        // save data and check for errors
        anp.save( function ( err ) {
            if ( err )
                res.send({ status: 300, message: 'Ops! Something is wrong...' });

            res.json({ status: 200, message: 'Data created!' });
        });
        
    })

    // get all data (accessed at GET http://localhost:8080/api/anp)
    .get( function ( req, res ) {
        Anp.find( function ( err, data ) {
            if ( err )
                res.send( { status: 300, message: 'Ops! Something is wrong...' } );

            res.json( data );
        })
        .cache()
        .exec(function() {
            // nothing
        });
        
    });

router.route('/anp/:anp_id')

    // get the data with that id (accessed at GET http://localhost:8080/api/anp/:anp_id)
    .get( function ( req, res ) {
        Anp.findById( req.params.anp_id, function ( err, anp ) {
            if ( err )
                res.send({ status: 300, message: 'Ops! Something is wrong...' });

            res.json( anp );
        });
    });

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'Welcome! :)' });   
});

module.exports = router;