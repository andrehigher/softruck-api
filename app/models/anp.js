// app/models/anp.js

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

var AnpSchema   = new Schema({
    name: String,
    cities: [{
    	name: String,
    	statistics: [{
    		type: { type: String },
    		consumerPrice: [{
    			averagePrice: Number,
    			standardDeviation: Number,
    			minPrice: Number,
    			maxPrice: Number,
    			averageMargin: Number
    		}],
    		distributionPrice: [{
    			averagePrice: Number,
    			standardDeviation: Number,
    			minPrice: Number,
    			maxPrice: Number
    		}]
    	}],
    	stations: [{
    		name: String,
    		address: String,
    		area: String,
    		flag: String,
    		prices: [{
    			type: { type: String },
    			sellPrice: Number,
    			buyPrice: Number,
    			saleMode: String,
    			provider: String,
    			date: Date
    		}]
    	}]
    }],
    dates: {
    	from: Date,
		to: Date
    }
});

module.exports = mongoose.model('Anp', AnpSchema);