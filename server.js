// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express'),
	app        = express(),
	bodyParser = require('body-parser'),
	mongoose   = require('mongoose');

// connecting to database
mongoose.connect('mongodb://localhost:27017/softruck');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// set our port
var port = process.env.PORT || 8080;        

// ROUTES FOR OUR API
var router = require('./app/routers/router');

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);